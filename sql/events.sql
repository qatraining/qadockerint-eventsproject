CREATE DATABASE IF NOT EXISTS `events` ;
USE `events`;


CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `email` varchar(20) NOT NULL,
  `staff_account` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

INSERT INTO `user` VALUES
(1, 'kat', 'password', 'kat@kat.com', 0),
(2, 'test', 'test', '', 0);

grant all privileges on *.* to 'phpuser'@'%' identified by 'abc123';
